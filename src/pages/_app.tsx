// MODULES
import type { AppProps } from 'next/app';
import { Global, ThemeProvider } from '@emotion/react';

// COMPONENTS
import { Footer } from 'src/components/navigation/footer/footer';
import { Header } from 'src/components/navigation/header/header';

// STYLES
import { globalStyles } from 'src/styles/global.styles';
import { theme } from 'src/styles/theme.style';

/**
 * Application root component
 */
function MyApp({ Component, pageProps }: AppProps): JSX.Element {
	return (
		<ThemeProvider theme={theme}>
			<Global styles={globalStyles} />
			<Header/>
			<main>
				<Component {...pageProps} />
			</main>
			<Footer/>
		</ThemeProvider>
	);
}

export default MyApp;

// TODO: Discuss code quality standards with your team
// TODO: Validate system requirements with stakeholder.
