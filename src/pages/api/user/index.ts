// MODULES
import { NextApiRequest, NextApiResponse } from 'next';

// RESOURCES
import { IUserMock } from 'src/types/user.types';
import { user } from 'src/mocks/user.mock';

export default function handler(rq: NextApiRequest, rs: NextApiResponse<IUserMock[]>) {
	rs.status(200).json([user('123')]);
}
