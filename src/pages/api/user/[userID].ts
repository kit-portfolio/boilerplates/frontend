// MODULES
import { NextApiRequest, NextApiResponse } from 'next';

// RESOURCES
import { IUserMock } from 'src/types/user.types';
import { user } from 'src/mocks/user.mock';

interface IQuery {
	userID: string;
}

export default function handler(rq: NextApiRequest, rs: NextApiResponse<IUserMock>) {
	const { userID } = rq.query as unknown as IQuery;

	rs.status(200).json(user(userID));
}
