// MODULES
import Head from 'next/head';
import type { NextPage } from 'next';
import { useCallback, useEffect } from 'react';

// COMPONENTS
import { Typography } from 'src/components/generics/typography/typography';

// RESOURCES
import { page } from 'src/constants/pages.constants';
import { useFetchUser } from 'src/controllers/user.controllers';

/**
 * Homepage
 */
const Home: NextPage = () => {
	const {
		isLoading,
		fetchUser,
		user,
	} = useFetchUser();

	useEffect(() => {
		console.log(`Loading: ${isLoading}`);
	}, [isLoading]);

	useEffect(() => {
		console.log((user) ? user : 'No user yet');
	}, [user]);

	const handleAPICall = useCallback(() => {
		console.log('Test API call');

		fetchUser('lorem_ipsum');
	}, [fetchUser]);

	return (
		<>
			<Head>
				<title>{page.home.title}</title>
				<meta name="description" content="Boilerplate"/>
			</Head>

			<Typography variant="Heading 1" onClick={() => handleAPICall()}>
                Behold the homepage!
			</Typography>
		</>
	);
};

export default Home;

// TODO: Add project description
// TODO: Remove Roboto if not needed
