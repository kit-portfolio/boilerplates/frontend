// MODULES
import '@emotion/react';

// RESOURCES
import { breakpoint } from 'src/constants/layout.constants';
import { TFontSize, TLayout } from 'src/styles/theme.style';

declare module '@emotion/react' {

	// Naming convention disabled because following interface redefines third-party interface
	// eslint-disable-next-line @typescript-eslint/naming-convention
	export interface Theme {
		breakpoint: typeof breakpoint,
		color: {
			primary: string,
			secondary: string,
			background: string,
			surface: string,
			text: string,
			error: string,
		},
		font: {
			roboto: string;
		}
		fontWeight: {
			bold: number;
			light: number;
			regular: number;
		},
		fontSize: Omit<{
			[layout in TLayout]: {
				[fontSize in TFontSize]: string;
			}
		}, 'tablet' | 'oversized' >,
		layout: Omit<{ [key in TLayout]: string }, 'mobile'>,
		spacing: {
			s: number;
			m: number;
			l: number;
			xl: number;
		}
	}
}

// TODO: make necessary changes to Theme type
