export interface IUserMock {
	id: string;
	firstname: string;
	lastname: string;
	age: number;
}

// TODO: Demonstrational asset. To be removed.
