// MODULES
import React from 'react';
import {
	Canvas,
	Description,
	Primary,
	Story,
	Title,
} from '@storybook/addon-docs';
import { ComponentMeta, ComponentStory } from '@storybook/react';

// COMPONENTS
import { Typography } from 'src/components/generics/typography/typography';

// RESOURCES
import { useScreenDetect } from 'src/utils/screen-detect.util';


export const ScreenDetect: ComponentStory<typeof Typography> = () => {
	const layout = useScreenDetect();

	return (
		<div>
			{Object.entries(layout).map(([key, value]) => (
				<Typography
					key={key}
					variant="Heading 3"
					css={{ color : (value) ? 'red' : 'black' }}>
					{`Layout ${key}`}
				</Typography>
			))}
		</div>
	);
};

export const MediaQueries: ComponentStory<typeof Typography> = () => (
	<div>
		<Typography
			variant="Heading 3"
			css={({ layout }) => ({
				textAlign : 'center',
				color : 'green',

				[layout.tablet] : { color : 'orange' },

				[layout.desktop] : { color : 'red' },

				[layout.oversized] : { color : 'cornflowerblue' },
			})}>
			Resize the screen to see the difference
		</Typography>
	</div>
);

export default {
	title : 'Utils',
	component : Typography,
	parameters : {
		viewMode : 'docs',
		previewTabs : { canvas : { hidden : true } },
		controls : { disable : true },
		docs : {
			page : () => (
				<>
					<Title>Screen detect</Title>
					<Description>
                        Screen detect util is a custom react hook dynamically querying screen params in order to define
                        layout type.
					</Description>
					<Description>
                        Resize screen to see the difference.</Description>
					<Primary/>
					<Title>Media query</Title>
					<Description>This story demonstrates how themed emotion media queries work.</Description>
					<Description>See code snippet for details.</Description>
					<Canvas>
						<Story id="utils--media-queries"/>
					</Canvas>
				</>
			),

		},
	},
} as ComponentMeta<typeof Typography>;

// TODO: Demonstrational asset. To be removed.
