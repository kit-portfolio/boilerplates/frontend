// MODULES
import { useForm } from 'react-hook-form';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import {
	Description,
	Primary,
	Title,
} from '@storybook/addon-docs';
import React, { useMemo } from 'react';

// COMPONENTS
import { Button } from 'src/components/generics/button/button';
import { Input } from 'src/components/generics/input/input';
import { Typography } from 'src/components/generics/typography/typography';

// RESOURCES
import { IUserMock } from 'src/types/user.types';
import { constraints } from 'src/constants/constraints.constants';
import { useValidation } from 'src/utils/validation.utils';

// STYLES
import { theme } from 'src/styles/theme.style';

export default {
	title : 'Form / Controller',
	component : Typography,
	parameters : {
		docs : {
			page : () => (
				<>
					<Title>Form controller</Title>
					<Description>
                        This story demonstrates how `react-hook-form` works. You can change data and submit form.
					</Description>
					<Description>
                        Form has validation rules. `Firstname` and `lastname` are limited by 45 characters, `age` has
                        min (18) and max (80) constraints.
					</Description>
					<Primary/>
				</>
			),

		},
	},
} as ComponentMeta<typeof Typography>;

export const Controller: ComponentStory<typeof Typography> = () => {
	const defaultValues = useMemo<Omit<IUserMock, 'id'>>(() => ({
		firstname : 'John',
		lastname : 'Doe',
		age : 18,
	}), []);

	const {
		register,
		handleSubmit,
		formState : { errors : formErrors },
	} = useForm({ defaultValues });
	const { isRequired, maxLength, minValue, maxValue } = useValidation();

	return (
		<div>
			<form
				css={{ display : 'flex', flexDirection : 'column', gap : 10, width : 300 }}
				onSubmit={handleSubmit((data) => console.log(data))}>
				<Input
					{...register('firstname', {
						...isRequired(),
						...maxLength(constraints.user.firstName.maxLength),
					})}
					label={'First name'}
					error={formErrors?.firstname?.message}
					placeholder="Enter first name"
					testId="firstname"
				/>
				<Input
					{...register('lastname', {
						...isRequired(),
						...maxLength(constraints.user.lastName.maxLength),
					})}
					label={'Last name'}
					error={formErrors?.lastname?.message}
					placeholder="Enter last name"
					testId="lastname"
				/>
				<Input
					{...register('age', {
						...isRequired(),
						...minValue(constraints.user.age.min),
						...maxValue(constraints.user.age.max),
					})}
					label={'Age'}
					error={formErrors?.age?.message}
					placeholder="Enter age"
					testId="age"
				/>
				<Button
					color={theme.color.primary}
					label="Submit"
					testId="submit"
					isStretched
					type="submit"
					css={{ marginTop : 10 }}/>
			</form>
		</div>
	);
};

// TODO: Demonstrational asset. To be removed.
