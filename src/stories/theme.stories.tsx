// MODULES
import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import {
	Primary,
	Story,
	Subtitle,
	Title,
} from '@storybook/addon-docs';

// COMPONENTS
import { Typography } from 'src/components/generics/typography/typography';

// RESOURCES
import { adaptiveColor } from 'src/utils/color.utils';

// STYLES
import { TTypography } from 'src/styles/typography.styles';
import { theme } from 'src/styles/theme.style';

const options: TTypography[] = ['Heading 1', 'Heading 2', 'Heading 3', 'Heading 4', 'Text', 'Caption'];

export default {
	title : 'Theme',
	component : Typography,
	parameters : {
		viewMode : 'docs',
		previewTabs : { canvas : { hidden : true } },
		controls : { disable : true },
		docs : {
			page : () => (
				<>
					<Title />
					<Subtitle>Typography exhibition</Subtitle>
					<Primary />
					<Subtitle>Theme palette</Subtitle>
					<Story id="theme--theme-palette"/>
				</>
			),

		},
	},
} as ComponentMeta<typeof Typography>;

export const typography: ComponentStory<typeof Typography> = () => (
	<div>
		{options.map((variant) => (
			<Typography key={variant} variant={variant} css={{ marginBottom : 12 }}>{variant}</Typography>
		))}
	</div>
);

export const themePalette: ComponentStory<typeof Typography> = () => (
	<div css={{ display : 'grid', gridTemplateColumns : '1fr 1fr 1fr', gap : 12 }}>
		{Object.entries(theme.color).map(([key, value]) => (
			<div
				key={key}
				css={({ fontSize, spacing }) => ({
					backgroundColor : value,
					color : adaptiveColor(value),
					padding : spacing.s,
					borderRadius : spacing.s / 2,
					textTransform : 'uppercase',
					textAlign : 'center',
					fontSize : fontSize.desktop.s,
					minWidth : 150,

					'&::before' : { content : `"${key}"` },

					'&:hover' : { '&::before' : { content : `"${value}"` } },
				})}/>
		))}
	</div>
);

// TODO: Demonstrational asset. To be removed.
