// MODULES
import { Interpolation, Theme } from '@emotion/react';

export const style: Interpolation<Theme> = ({ color, spacing }: Theme) => ({
	color : color.secondary,
	backgroundColor : color.primary,
	padding : '2rem',
	textAlign : 'center',

	display : 'flex',
	justifyContent : 'center',
	gap : spacing.xl,

	a : {
		color : color.secondary,
		textDecoration : 'none',
		textTransform : 'uppercase',
	},
});
