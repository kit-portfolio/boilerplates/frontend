// RESOURCES
import { testId } from './header.tests';

// COMPONENTS
import { Typography } from 'src/components/generics/typography/typography';

// RESOURCES
import { createTestId } from 'src/utils/test.utils';

// STYLES
import { style } from './header.styles';

/**
 * Generic header component
 */
function Header(): JSX.Element {
	return (
		<header css={style} {...createTestId(testId)}>
			<Typography variant="Heading 3">Header</Typography>
		</header>
	);
}

export { Header };
