// RESOURCES
import { Typography } from 'src/components/generics/typography/typography';
import { testId } from './footer.tests';

// RESOURCES
import { createTestId } from 'src/utils/test.utils';

// STYLES
import { style } from './footer.styles';

/**
 * Generic footer component
 */
function Footer(): JSX.Element {
	return (
		<footer css={style} {...createTestId(testId)}>
			<Typography variant="Heading 3">Footer</Typography>
		</footer>
	);
}

export { Footer };
