// MODULES
import { Interpolation, Theme } from '@emotion/react';

export const style: Interpolation<Theme> = ({ color }: Theme) => ({
	color : color.background,
	backgroundColor : color.primary,
	padding : '2rem',
	textAlign : 'center',
});
