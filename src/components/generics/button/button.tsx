// MODULES
import React, {
	ComponentPropsWithRef,
	MouseEvent,
} from 'react';

// RESOURCES
import { createTestId } from 'src/utils/test.utils';

// STYLES
import { style } from 'src/components/generics/button/button.styles';

export interface IButtonProps extends ComponentPropsWithRef<'button'> {

	/**
	 * Button label text.
	 */
	label: string;

	/**
	 * Identifier to be used for button references in testing.
	 */
	testId: string;

	/**
	 * Button background color.
	 */
	color: string;

	/**
	 * Click handler.
	 */
	onClick?: (e: MouseEvent<HTMLButtonElement>) => void;

	/**
	 * Parameter defining whether button will occupy all horizontal space available.
	 */
	isStretched?: boolean;

	/**
	 * Parameter defining whether button is disabled.
	 */
	isDisabled?: boolean;
}

const Button = React.forwardRef<HTMLButtonElement, IButtonProps>(
	(
		{
			isStretched = false,
			isDisabled = false,
			testId,
			color,
			label,
			...props
		}: IButtonProps
	) => (
		<button
			{...createTestId(testId)}
			css={style(color, isStretched)}
			{...props}
			disabled={isDisabled}
		>
			{label}
		</button>
	)
);

Button.displayName = 'Button';

export { Button };

// TODO: Demonstrational asset. To be removed.
