// MODULES
import React from 'react';
import { Meta, Story } from '@storybook/react';

// RESOURCES
import { theme } from 'src/styles/theme.style';
import { Button as ButtonComponent, IButtonProps } from 'src/components/generics/button/button';

// Local types
type ButtonStoryI = Story<IButtonProps>;

const argumentSet: IButtonProps = {
	label : 'Fancy button',
	testId : 'fancy-button',
	color : theme.color.secondary,
	onClick : () => console.log('Clicked'),
	isStretched : false,
	isDisabled : false,
};

export default {
	title : 'Form / Button',
	component : ButtonComponent,
	args : argumentSet,
} as Meta;

const Template: ButtonStoryI = (args) => <ButtonComponent {...args} />;

export const Button = Template.bind({});

// TODO: Demonstrational asset. To be removed.
