// MODULES
import { FunctionInterpolation, Theme } from '@emotion/react';

// RESOURCES
import { adaptiveColor, disabled } from 'src/utils/color.utils';

const style: (backgroundColor: string, isStreched: boolean) => FunctionInterpolation<Theme> =
    (backgroundColor, isStreched) => {
		return ({
			fontSize,
			spacing,
		}: Theme) => ({
			padding : spacing.s,
			color : adaptiveColor(backgroundColor),
			borderRadius : spacing.s / 2,
			border : 'none',
			fontSize : fontSize.mobile.s,
			textAlign : 'center',
			textTransform : 'uppercase',
			textDecoration : 'none',
			cursor : 'pointer',
			userSelect : 'none',
			appearance : 'none',
			width : (isStreched) ? '100%' : 'initial',
			backgroundColor,
			transition : '0.3s',

			':hover' : {
				boxShadow : '0px 0px 5px 2px rgba(0,0,0,0.3)',
				transition : '0.3s',
			},

			':disabled' : { backgroundColor : disabled(backgroundColor) },
		});
    };

export { style };

// TODO: Demonstrational asset. To be removed.
