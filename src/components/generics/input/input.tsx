// MODULES
import React, { ComponentPropsWithRef, useMemo } from 'react';

// RESOURCES
import { Typography } from 'src/components/generics/typography/typography';
import { createTestId } from 'src/utils/test.utils';

// STYLES
import { style } from 'src/components/generics/input/input.styles';

export interface IInputProps extends ComponentPropsWithRef<'input'> {

	/**
	 * Input label to be used for references in UI.
	 */
	label?: string;

	/**
	 * Input name to be used for references in code.
	 */
	name: string;

	/**
	 * Input placeholder.
	 */
	placeholder: string;

	/**
	 * Input's error message.
	 */
	error?: string;

	/**
	 * Identifier to be used for input references in testing.
	 */
	testId: string;
}

const Input = React.forwardRef<HTMLInputElement, IInputProps>(
	(
		{
			error,
			label = '',
			name,
			placeholder,
			value,
			testId,
			...props
		}: IInputProps,
		ref
	) => {

		const renderLabel = useMemo(() => {
			if (!label) return null;

			return (
				<label htmlFor={name} {...createTestId(`${testId}-label`)}>
					<Typography variant="Text" css={style.label}>
						{label}
					</Typography>
				</label>
			);
		}, [label, testId, name]);

		const renderError = useMemo(() => {
			if (!error) return null;

			return (
				<div {...createTestId(`${testId}-error`)}>
					<Typography tag="p" variant="Caption" css={style.error}>
						{error}
					</Typography>
				</div>
			);
		}, [error, testId]);

		return (
			<div>
				{renderLabel}
				<input
					{...createTestId(testId)}
					placeholder={placeholder}
					ref={ref}
					name={name}
					css={style.generic}
					id={name}
					value={value}
					{...props}
				/>
				{renderError}
			</div>
		);
	}
);

Input.displayName = 'Input';

export { Input };

// TODO: add Styles to typography
