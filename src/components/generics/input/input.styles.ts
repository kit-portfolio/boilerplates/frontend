// MODULES
import { Interpolation, Theme } from '@emotion/react';

const error: Interpolation<Theme> = ({
	color,
	spacing,
}: Theme) => ({
	color : color.error,
	margin : `${spacing.s / 2}px ${spacing.s}px`,
});

const generic: Interpolation<Theme> = ({
	color,
	fontSize,
	spacing,
}: Theme) => ({
	width : '100%',
	color : color.text,
	boxSizing : 'border-box',
	padding : spacing.s,
	borderRadius : spacing.s,
	border : `1px solid ${color.text}`,
	fontSize : fontSize.desktop.s,
});

const label: Interpolation<Theme> = ({ spacing }: Theme) => ({ margin : `${spacing.s / 2}px ${spacing.s}px` });


export const style = {
	error,
	generic,
	label,
};
