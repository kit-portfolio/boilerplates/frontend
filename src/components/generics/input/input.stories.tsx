// MODULES
import React from 'react';
import { Meta, Story } from '@storybook/react';

// RESOURCES
import { IInputProps, Input as InputComponent } from './input';

// Local types
type ButtonStoryI = Story<IInputProps>;

const argumentSet: IInputProps = {
	name : 'sample',
	label : 'Fancy label',
	placeholder : 'Value goes here',
	testId : 'sample-input',
};

export default {
	title : 'Form / Input',
	component : InputComponent,
	args : argumentSet,
} as Meta;

const Template: ButtonStoryI = (args) => <InputComponent {...args} />;

export const Input = Template.bind({});

// TODO: Demonstrational asset. To be removed.
