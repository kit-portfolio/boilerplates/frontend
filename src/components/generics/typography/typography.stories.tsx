// MODULES
import { ComponentMeta, ComponentStory } from '@storybook/react';

// RESOURCES
import { Typography as TypographyComponent } from 'src/components/generics/typography/typography';

// STYLES
import { TTypography } from 'src/styles/typography.styles';

const options: TTypography[] = ['Heading 1', 'Heading 2', 'Heading 3', 'Heading 4', 'Text', 'Caption'];

export default {
	title : 'Components/Typography',
	component : TypographyComponent,
	argTypes : {
		variant : {
			options,
			control : { type : 'inline-radio' },
		},
	},
} as ComponentMeta<typeof TypographyComponent>;

const Template: ComponentStory<typeof TypographyComponent> = ({ children, ...args }) => (
	<TypographyComponent {...args}>{children}</TypographyComponent>
);

export const Typography = Template.bind({});
Typography.args = {
	children : 'Lorem ipsum dolor sit amet',
	variant : 'Heading 1',
};
