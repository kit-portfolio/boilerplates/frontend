// MODULES
import axios from 'axios';
import { useState } from 'react';

// RESOURCES
import { IUserMock } from 'src/types/user.types';
import { api } from 'src/constants/api.constants';

export function useFetchUser() {
	const [isLoading, setLoading] = useState(false);
	const [user, setUser] = useState<IUserMock | null>(null);

	function fetchUser(userID: string) {
		setLoading(true);

		axios
			.get(api.user.getByUID(userID))
			.then(({ data }) => setUser(data))
			.catch((e) => console.log(e))
			.finally(() => setLoading(false));
	}

	return { isLoading, fetchUser, user };
}

// TODO: Demonstrational asset. To be removed.
