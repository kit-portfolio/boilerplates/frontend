// MODULES
import { css } from '@emotion/react';
import emotionNormalize from 'emotion-normalize';

const globalStyles = css`
	${emotionNormalize} 
	
	#__next {

		// Layout
		display: flex;
		flex-direction: column;
		gap: 20px;
		justify-content: space-between;
		min-height: 100vh;

		// Styling
		user-select: none;
	};
	
	body {
		font-family: 'Roboto', sans-serif;
	}

	html { 
		font-size: 10px;
	}
	
	main {
		flex-grow: 1;

		align-items: center;
		display: flex;
		justify-content: center;
	},

h1,h2,h3,h4 {
	margin: 0;
};
`;

export { globalStyles };

// TODO: review these styles, remove what is extra to you
