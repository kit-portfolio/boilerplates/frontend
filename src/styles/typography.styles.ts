// MODULES
import { Interpolation, Theme } from '@emotion/react';

export type TTypography = 'Heading 1' | 'Heading 2' | 'Heading 3' | 'Heading 4' | 'Text' | 'Caption';

const heading1: Interpolation<Theme> = ({ font, fontSize, fontWeight, layout }: Theme) => ({
	fontFamily : font.roboto,
	fontSize : fontSize.mobile.xxl,
	fontWeight : fontWeight.bold,

	[layout.tablet] : { fontSize : fontSize.desktop.xxl },
});

const heading2: Interpolation<Theme> = ({ font, fontSize, fontWeight, layout }: Theme) => ({
	fontFamily : font.roboto,
	fontSize : fontSize.mobile.xl,
	fontWeight : fontWeight.bold,

	[layout.tablet] : { fontSize : fontSize.desktop.xl },
});

const heading3: Interpolation<Theme> = ({ font, fontSize, fontWeight, layout }: Theme) => ({
	fontFamily : font.roboto,
	fontSize : fontSize.mobile.l,
	fontWeight : fontWeight.bold,

	[layout.tablet] : { fontSize : fontSize.desktop.l },
});

const heading4: Interpolation<Theme> = ({ font, fontSize, fontWeight, layout }: Theme) => ({
	fontFamily : font.roboto,
	fontSize : fontSize.mobile.m,
	fontWeight : fontWeight.bold,

	[layout.tablet] : { fontSize : fontSize.desktop.m },
});

const text: Interpolation<Theme> = ({ font, fontSize, fontWeight, layout }: Theme) => ({
	fontFamily : font.roboto,
	fontSize : fontSize.mobile.s,
	fontWeight : fontWeight.regular,

	[layout.tablet] : { fontSize : fontSize.desktop.s },
});

const caption: Interpolation<Theme> = ({ font, fontSize, fontWeight, layout }: Theme) => ({
	fontFamily : font.roboto,
	fontSize : fontSize.mobile.xs,
	fontWeight : fontWeight.light,

	[layout.tablet] : { fontSize : fontSize.desktop.xs },
});

export { heading1, heading2, heading3, heading4, text, caption };

// TODO: define typography and its typings
