// MODULES
import { Theme } from '@emotion/react';

// RESOURCES
import { breakpoint } from 'src/constants/layout.constants';
import { palette } from 'src/styles/palette.styles';

export const theme: Theme = {
	breakpoint,
	color : {
		primary : palette.blue_usafa,
		secondary : palette.heat_wave,
		background : palette.white,
		surface : palette.gray_cultured,
		text : palette.black_eerie,
		error : palette.red_imperial,
	},
	font : { roboto : 'Roboto, sans-serif' },
	fontWeight : {
		light : 300,
		regular : 400,
		bold : 700,
	},
	fontSize : {
		mobile : {
			xs : '1rem',
			s : '1.2rem',
			m : '1.5rem',
			l : '1.7rem',
			xl : '2rem',
			xxl : '3rem',
		},
		desktop : {
			xs : '1.2rem',
			s : '1.5rem',
			m : '1.7rem',
			l : '2rem',
			xl : '3rem',
			xxl : '5rem',
		},
	},
	layout : {
		tablet : `@media (min-width: ${breakpoint.tablet}px)`,
		desktop : `@media (min-width: ${breakpoint.desktop}px)`,
		oversized : `@media (min-width: ${breakpoint.maxWidth + 1}px)`,
	},
	spacing : {
		s : 12,
		m : 18,
		l : 24,
		xl : 36,
	},
} as const;

export type TFontSize = 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';

export type TLayout = 'mobile' | 'tablet' | 'desktop' | 'oversized';

export type TFontWeight = keyof typeof theme.fontWeight;

export type TFont = keyof typeof theme.font;

// TODO: define color scheme
// TODO: define font set
// TODO: define layout types
// TODO: define spacings
// TODO: define typings for theme
