export const palette = {
	black_eerie : '#1A2027',
	blue_usafa : '#004E98',
	gray_cultured : '#F5F5F5',
	heat_wave : '#FF7B00',
	red_imperial : '#EF233C',
	white : 'white',
} as const;

// TODO: Define your project palette
