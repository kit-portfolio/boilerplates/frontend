// RESOURCES
import { IUserMock } from 'src/types/user.types';

export const user = (id: string): IUserMock => ({
	id,
	firstname : 'John',
	lastname : 'Doe',
	age : 22,
});

// TODO: Demonstrational asset. To be removed.
