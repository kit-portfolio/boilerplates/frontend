// MODULES
import { ValidationRule } from 'react-hook-form';

// RESOURCES
import { constraints } from 'src/constants/constraints.constants';

const messages = {
	incorrectPattern : 'Incorrect format.',
	isRequired : 'This is a required field.',
	maxLength : 'The length of this field is limited by {{limit}} characters.',
	minLength : 'The length of this field must be at least {{limit}} characters.',
	maxValue : 'This field maximum value is {{limit}}.',
	minValue : 'This field minimum value is {{limit}}',
};

export function useValidation() {

	// =========================
	// COMMON VALIDATION METHODS
	// =========================

	interface IRequiredRules {
		required: ValidationRule<boolean>,
		setValueAs: ((v: string | number) => string | number) | undefined,
	}

	/**
     * @param message - custom validation message.
     */
	const isRequired: (message?: string) => IRequiredRules =
        (message = messages.isRequired) => ({
			setValueAs : v => (typeof v === 'string' ? v.trim() : v),
			required : {
				message,
				value : true,
			},
        });

	/**
     * @param limit - number of symbols limiting user input.
     * @param message - custom validation message.
     */
	const maxLength: (limit: number, message?: string) => { maxLength: ValidationRule<number> } =
        (limit, message = messages.maxLength) => ({
			maxLength : {
				message,
				value : limit,
			},
        });

	/**
     * @param limit - number of symbols limiting user input.
     * @param message - custom validation message.
     */
	const minLength: (limit: number, message?: string) => { minLength: ValidationRule<number> } =
        (limit, message = messages.minLength) => ({
			minLength : {
				message,
				value : limit,
			},
        });

	/**
     * @param limit - maximum allowed value.
     * @param message - custom validation message.
     */
	const maxValue: (limit: number, message?: string) => { max: ValidationRule<number> } =
        (limit, message = messages.maxValue) => ({
			max : {
				message,
				value : limit,
			},
        });

	/**
     * @param limit - minimum allowed value.
     * @param message - custom validation message.
     */
	const minValue: (limit: number, message?: string) => { min: ValidationRule<number> } =
        (limit, message = messages.minValue) => ({
			min : {
				message,
				value : limit,
			},
        });

	/**
     * @param pattern - RegExp used to validate input format.
     * @param message - custom validation message.
     */
	const validatePattern: (pattern: RegExp, message?: string) => { pattern: ValidationRule<RegExp> } =
        (pattern, message = messages.incorrectPattern) => ({
			pattern : {
				message,
				value : pattern,
			},
        });

	// ==================
	// VALIDATION PRESETS
	// ==================

	interface IPasswordValidationRules {
		max: ValidationRule<number>;
		min: ValidationRule<number>;
	}
	const validateAge: () => IPasswordValidationRules =
        () => ({
			...maxValue(constraints.user.age.max),
			...minValue(constraints.user.age.min),
        });

	return {
		isRequired,
		maxLength,
		minLength,
		maxValue,
		minValue,
		validateAge,
		validatePattern,
	};
}
