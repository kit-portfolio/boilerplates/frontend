interface IConstraintEnum {
	readonly max: number;
	readonly min: number;
	readonly maxLength: number;
	readonly minLength: number;
	readonly pattern: RegExp;
}

interface ICommon {
	email: Pick<IConstraintEnum, 'pattern'>;
	phone: Pick<IConstraintEnum, 'pattern'>;
	url: Pick<IConstraintEnum, 'pattern'>;
}

const common: ICommon = {

	/**
     * PATTERN REFERENCE
     * The recipient's name is limited to 64 symbols.
     * The domain name is limited to 253 symbols.
     * Numbers allowed.
     * Uppercase letters are allowed.
     * Email should have recipient name, @ symbol, domain name, and first-level domain name.
     */
	// eslint-disable-next-line max-len
	email : { pattern : /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },

	/**
     * PATTERN REFERENCE
     * Only numbers are allowed.
     * Plus prefix allowed. Example: +1234567890 (OK)
     * Brackets allowed. Example: (123)4567890 (OK)
     * Minimum length 10 digits
     * Maximum length 12 digits
     * Only digits allowed. Example: 123444556a (NOT OK)
     * Plus prefix should be exactly first. Example: (+123)4567890 (NOT OK)
     * Special symbols are not allowed. Example: 123^444*55=66 (NOT OK)
     *
     * SUGGESTIONS
     * Space separators allowed. Example: 123 444 55 66 (OK)
     * Space separators should be correctly placed. Example: 1 2 3 4 4 4 5 5 6 6 (NOT OK)
     * Dash separators allowed. Example: 123 444-55-66 (OK)
     * Phone number brackets should always be paired. Example: (1234445566 (NOT OK) || 123)4445566 (NOT OK)
     */
	// eslint-disable-next-line no-useless-escape
	phone : { pattern : /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im },
	// eslint-disable-next-line
    url: { pattern: /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g, },
};

interface IUser {
	readonly age: Pick<IConstraintEnum, 'min' | 'max'>;
	readonly firstName: Pick<IConstraintEnum, 'maxLength'>;
	readonly lastName: Pick<IConstraintEnum, 'maxLength'>;
}

const user: IUser = {
	age : { min : 18, max : 80 },
	firstName : { maxLength : 45 },
	lastName : { maxLength : 45 },
};

export const constraints = { common, user };
