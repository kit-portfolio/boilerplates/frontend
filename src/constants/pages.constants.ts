interface IPage {

	/* Page title */
	title: string;

	/* Page route */
	route: string;

	/* Switch defining whether the page should appear on the generated sitemap */
	isPublic: boolean;
}

/**
 *  Object containing all the meta information about application pages
 *  Use cases:
 *  - consistent application routing
 *  - consistent application pages titling
 *  - automatic sitemap generation
 */
export const page = {
	home : {
		title : 'Home',
		route : '/',
		isPublic : true,
	},

	// Placeholder for TBD pages
	placeholder : {
		title : 'Placeholder page',
		route : '#',
		isPublic : true,
	},
} as const;

// TODO: define project home title
