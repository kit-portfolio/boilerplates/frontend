const root = 'api';
const user = 'user';

export const api = {
	user : {
		getByUID : (id: string) => `/${root}/${user}/${id}`,
		list : `/${root}/${user}/`,
	},
} as const;

// TODO: remove demonstrational API endpoints
