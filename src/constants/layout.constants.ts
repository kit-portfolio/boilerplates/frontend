export const breakpoint = {
	minWidth : 320,
	tablet : 480,
	desktop : 768,
	maxWidth : 1440,
};

// TODO: define breakpoints
// BE ADVISED: don't forget to actualise theme media queries and screen detect util if you gonna change layout variants
