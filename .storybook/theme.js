import { create } from '@storybook/theming';

export default create({
    base: 'light',
    brandTitle: 'My custom storybook',
    brandUrl: 'https://example.com',
    brandImage: 'https://place-hold.it/350x150',
    brandTarget: '_blank',
});

// TODO: Redefine properties to brand your storybook
