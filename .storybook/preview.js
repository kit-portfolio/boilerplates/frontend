// MODULES
import { Global } from '@emotion/react';
import {ThemeProvider} from "@emotion/react";

// STYLES
import { theme } from 'src/styles/theme.style';
import {globalStyles} from "src/styles/global.styles";
import { palette } from "src/styles/palette.styles";

const customViewports = {
    kindleFire2: {
        name: 'Phone',
        styles: {
            width: '600px',
            height: '963px',
        },
    },
    kindleFireHD: {
        name: 'Kindle Fire HD',
        styles: {
            width: '533px',
            height: '801px',
        },
    },
};

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
    viewport: { viewports: customViewports },
    colorPicker: {
        palettes: [
            {
                name: 'Palette',
                palette: palette,
            },
        ]
    },
};

const withTheme = (StoryFn) => {
    return (
        <ThemeProvider theme={theme}>
            <Global styles={globalStyles} />
            <StoryFn />
        </ThemeProvider>
    )
}

export const decorators = [withTheme];
