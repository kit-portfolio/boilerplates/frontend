# TODO

### Storybook
- [x] Redo stories

### Tech
- [x] Setup form
- [x] Setup validation
- [x] Fix lint issues
- [x] Fix utils
- [x] Fix form controller story
- [x] Button component
- [x] Polished effects
- [x] Describe interfaces in stories

### Documentation
- [x] Create doc for project structure
- [x] Describe storybook and app start scripts

### Demonstration pages
  - [x] Form controller
  - [x] Media queries
  - [x] Detect screen hook
  - [x] Theme
  - [x] Palette
  - [x] Typography
