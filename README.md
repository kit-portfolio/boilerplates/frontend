`NextJs` app template containing the most common tools.  
To start the project all you need is just resolve all TODO's, and you're ready to go.

# Tech stack
* Typescript
* NextJS
* Emotion
* Storybook
* React hook forms
* EsLint

# Contents
* TECH
  * Fully initialised `NextJS` app.
  * Configured `Typescript`
  * Configured `EsLint` along with set of rules
  * Fully configured `Storybook`
  * Configured `Emotion` with theme-based styling
  * Common utils
  * Initialised form controller with custom validation presets
* DOCUMENTATION
  * Code quality manifesto
  * System requirements draft
  * TODO-based starting guide
* TEMPLATES
  * Routing 
  * API controllers
  * Components
  * Styles

# Working with template
### Launch
Nothing interesting here - only a dummy app.
```bash
yarn app:start:dev
```
App will be hosted on [localhost:3000](http://localhost:3000)

### Storybook
In project Storybook you can find some interactive demos.
```bash
yarn storybook:start
```
Storybook will be hosted on [localhost:6006](http://localhost:6006)
